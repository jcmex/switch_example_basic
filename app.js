var express = require('express'),
    app = express(),
    bodyParser = require('body-parser')

var dateFormat = require('dateformat');

app.use(bodyParser.json());

// Handler for internal server errors
function errorHandler(err, req, res, next) {
    console.error(err.message);
    console.error(err.stack);
    res.status(500).send('error_template', { error: err });
}

var switch_state = "Waiting";
var day=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");

app.get('/last', function(req, res, next) {
    console.log(day);
    res.json({state: switch_state, timestamp: day });
});

app.post('/save_state', function(req, res, next) {
    if (req.body != 'undefined') {
      console.log(req.body);
      var current_state = req.body.state;
    }
    if (typeof current_state == 'undefined' || req.body == 'undefined') {
        console.log('Please send switch state!');
        res.status(500).send("undefined state");
    }
    else {
        switch_state = current_state;
        day=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
        res.status(200).end("the current state is " + switch_state);
    }
});

app.use(errorHandler);

var server = app.listen(3000, function() {
    var port = server.address().port;
    console.log('Express server listening on port %s.', port);
});
